const listAnime = [
    {
        judul: "Kimi No Nawa",
        genre: "Romance"
    },
    {
        judul: "One Piece",
        genre: "Action"
    },
    {
        judul: "Jujutsu Kaisen",
        genre: "Supernatural"
    },
    {
        judul: "Overlord",
        genre: "Isekai"
    },
    {
        judul: "Naruto",
        genre: "Action"
    },
    {
        judul: "Shigeki No Kyojin",
        genre: "Drama"
    }
]

for (let i = 0; i < listAnime.length; i++) {
    console.log(listAnime[i]["judul"],
        ("genre " + listAnime[i]["genre"]));
}

console.log("-cari-")
function cariAnime(judul) {
    for (let i = 0; i < listAnime.length; i++) {
        if (listAnime[i]["judul"] == judul) {
            console.log(listAnime[i]["judul"]), ("genre " + listAnime[i]["genre"]);
        }
    }
}
cariAnime("Kimi No Nawa");

console.log("-filter-");
function filterAnime(genre) {
    for(let i = 0; i < listAnime.length; i++){
        if (listAnime[i]["genre"] == genre){
            console.log(listAnime[i]["judul"]);
        }
    }
}
filterAnime("Romance")